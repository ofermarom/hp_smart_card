/*
   Confidential computer software. Valid license from HP required for possession, use or copying.  Consistent with FAR 12.211 and 12.212, Commercial Computer Software, Computer Software Documentation, and Technical Data for Commercial Items are licensed to the U.S. Government under vendor's standard commercial license.

   THE LICENSED SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY DESCRIPTION.  HP SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  YOU ASSUME THE ENTIRE RISK RELATING TO THE USE OR PERFORMANCE OF THE LICENSED SOFTWARE.

   HP Company Confidential
   © Copyright 2009-2015 HP Development Company, L.P.
   Made in U.S.A.
 */

//*****************************************************************************
// File : hp_smart_card_gpio_ifc.h
//-----------------------------------------------------------------------------
// Description: Fake GPIO driver.
//
//*****************************************************************************
#include <stdio.h>

#include "hp_smart_card_gpio_ifc.h"

void HP_SMART_CARD_gpio_init()
{
    printf("Initialized Fake gpio driver.\n");
    return;
}

HP_SMART_CARD_bool_t HP_SMART_CARD_gpio_get_value(HP_SMART_CARD_gpio_line_t line)
{
    printf("Fake GPIO driver: HP_SMART_CARD_gpio_get_value()\n");
    return(HP_SMART_CARD_FALSE);
}


void HP_SMART_CARD_gpio_set_value(HP_SMART_CARD_gpio_line_t line, HP_SMART_CARD_bool_t value)
{
    printf("Fake GPIO driver: HP_SMART_CARD_gpio_set_value()\n");
    return;
}
