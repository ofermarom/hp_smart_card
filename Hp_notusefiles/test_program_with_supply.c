/*
   Confidential computer software. Valid license from HP required for possession, use or copying.  Consistent with FAR 12.211 and 12.212, Commercial Computer Software, Computer Software Documentation, and Technical Data for Commercial Items are licensed to the U.S. Government under vendor's standard commercial license.

   THE LICENSED SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY DESCRIPTION.  HP SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  YOU ASSUME THE ENTIRE RISK RELATING TO THE USE OR PERFORMANCE OF THE LICENSED SOFTWARE.

   HP Company Confidential
   � Copyright 2009-2016 HP Development Company, L.P.
   Made in U.S.A.
 */

#include <stdio.h>

#include "hp_smart_card.h"
#include "hp_os_service.h"

#include "hp_smart_card_gpio_ifc.h"
#include "hp_smart_card_i2c_ifc.h"


void print_returns(int result)
{
    HW_SMART_CARD_status_t status = LIB_HP_SMART_CARD_last_status();
    if (HW_SMART_CARD_success_e == status)
        printf("Result = %s (%d)\n",
               LIB_HP_SMART_CARD_result_string(result), result);
    else
        printf("Result = %s (%d)  Status = %s (%d)\n",
               LIB_HP_SMART_CARD_result_string(result), result,
               LIB_HP_SMART_CARD_status_string(status), status);
}


void assert_handler(const char *error_str)
{
    printf("=========================================\n");
    printf("Test Main: HP_ASSERT Failed\n");
    printf("%s\n", error_str);
    printf("=========================================\n");
}

void cache_monitor_failure_handler(HP_SMART_CARD_device_id_t dev_id,
                                   HP_SMART_CARD_result_t result)
{
    printf("=========================================\n");
    printf("Test Main: Cache monitor failure\n");
    printf("Device Id = %d, ", dev_id);
    print_returns(result);
    printf("=========================================\n");
}


int check_result(int result, char* msg)
{
    if ((int) HP_SMART_CARD_OK == result)
        return 0;
    printf(msg);
    print_returns(result);
    return -1;
}



int main()
{
    HP_SMART_CARD_result_t  result;
    unsigned char           family_id;
    uint16_t                u16;
    uint32_t                u32;
    char                    buffer[31];
    HP_SMART_CARD_field_t   field;
    int                     component_id;
    unsigned char           expected_family_id;

    // Set logging level
    LIB_HP_SMART_CARD_set_log_depth(0);

    // Initialize GPIO and I2C drivers
    HP_SMART_CARD_gpio_init();
    HP_SMART_CARD_i2c_init();

    // Initialise the library
    printf(">>> HP SMART CARD Library version = %s\n", LIB_HP_SMART_CARD_get_version());
    LIB_HP_SMART_CARD_init();

    // Un-comment the next line to show CUSTOMER logging
    //LIB_HP_SMART_CARD_set_log_mask(HP_DBG_LEVEL_CUSTOMER);

    // Register for assert callback
    LIB_HP_SMART_CARD_register_assert_callback(assert_handler);

    // Register for cache monitor callack
    LIB_HP_SMART_CARD_register_cache_monitor_callback(cache_monitor_failure_handler);


    // @@@ Check for presence of Host smart card @@@

    result = LIB_HP_SMART_CARD_device_present(HP_SMART_CARD_DEVICE_HOST);
    if (check_result(result, ">>> LIB_HP_SMART_CARD_device_present(HP_SMART_CARD_DEVICE_HOST): NOT PRESENT.  "))
        return -1;
    else
        printf(">>> LIB_HP_SMART_CARD_device_present(HP_SMART_CARD_DEVICE_HOST): Present\n");


    // ----- Loop through both Pen and Supply Component smart cards -----

    for (component_id = HP_SMART_CARD_DEVICE_ID_0;/* component_id <= HP_SMART_CARD_DEVICE_ID_1*/; component_id++)
    {
        printf("----- TESTING HP_SMART_CARD_DEVICE_ID_");
        if (component_id == HP_SMART_CARD_DEVICE_ID_0)
            printf("0 Pen -----\n");
       /* if (component_id == HP_SMART_CARD_DEVICE_ID_1)
            printf("1 Supply -----\n");*/


        // @@@ Check for presence of Component smart card @@@

        result = LIB_HP_SMART_CARD_device_present(component_id);
        if (check_result(result, ">>> LIB_HP_SMART_CARD_device_present(): NOT PRESENT.  "))
            return -1;
        else
            printf(">>> LIB_HP_SMART_CARD_device_present(): Present\n");


        // @@@ Initialize the Component smart card @@@

        result = LIB_HP_SMART_CARD_device_init(component_id);
        if (check_result(result, ">>> LIB_HP_SMART_CARD_device_init().  "))
            return -1;
        else
            printf(">>> LIB_HP_SMART_CARD_device_init(): Successful\n");


        // @@@ Read the Family ID @@@

        result = LIB_HP_SMART_CARD_read_field(component_id,
                                              HP_SMART_CARD_FAMILY_ID,
                                              sizeof(family_id),
                                              &family_id);
        if (check_result(result, ">>> Failed to read Family ID.  "))
            return -1;
        else
            printf(">>> Family ID = %d\n", family_id);
        // validate the Family ID for device ID
        expected_family_id = (component_id == HP_SMART_CARD_DEVICE_ID_0 ? HP_SMART_CARD_INK_FAMILY_ID : HPSCS_FAMILY_ID);
        if (family_id != expected_family_id)
        {
            printf(">>> ERROR: Family ID %d is expected for device id %d.\n", expected_family_id, component_id);
            return -1;
        }


        // @@@ Read printer lockdown partition number (field depends on Family ID) @@@

        field   = (HP_SMART_CARD_field_t) (family_id == HPSCS_FAMILY_ID ? HPSCS_PRINTER_LOCK_DOWN_PARTITION : HP_SMART_CARD_INK_PRINTER_LOCK_DOWN_PARTITION);
        result  = LIB_HP_SMART_CARD_read_field(component_id,
                                               field,
                                               sizeof(u16),
                                               &u16);
        if (check_result(result, ">>> Failed to read Printer Lockdown Partition.  "))
            return -1;
        else
            printf(">>> Printer Lockdown Partition = %d\n", u16);


        // @@@ Write and flush MRU Platform ID @@@

        field   = (HP_SMART_CARD_field_t) (family_id == HPSCS_FAMILY_ID ? HPSCS_MRU_PLATFORM_ID_CHAR_1 : HP_SMART_CARD_INK_MRU_PLATFORM_ID_CHAR_1);
        result  = LIB_HP_SMART_CARD_write_string(component_id,
                                                 field,
                                                 12,
                                                 "abcdefgh1234");
        if (check_result(result, ">>> Failed to write MRU Platform ID.  "))
            return -1;
        HP_USLEEP(1000000);                 // 1 second delay
        result = LIB_HP_SMART_CARD_flush();
        if (check_result(result, ">>> Failed to flush.  "))
            return -1;


        // @@@ Read, increment, and write the OEM Defined RW Field 1 (field depends on Family ID) @@@

        field   = (HP_SMART_CARD_field_t) (family_id == HPSCS_FAMILY_ID ? HPSCS_OEM_DEFINED_RW_FIELD_1 : HP_SMART_CARD_INK_OEM_DEF_RW_FIELD_1);
        result  = LIB_HP_SMART_CARD_read_field(component_id,
                                               field,
                                               sizeof(u32),
                                               &u32);
        if (check_result(result, ">>> Failed to read OEM Defined RW Field 1.  "))
            return -1;
        u32++;      // increment
        result = LIB_HP_SMART_CARD_write_field(component_id,
                                               field,
                                               sizeof(u32),
                                               &u32);
        if (check_result(result, ">>> Failed to read OEM Defined RW Field 1.  "))
            return -1;
        else
            printf(">>> OEM Defined RW Field 1 incremented = %d\n", u32);
        // NOTE: data has not been flushed (single-threaded)


        // @@@ Write and flush MRU Platform ID a second time @@@

        field   = (HP_SMART_CARD_field_t) (family_id == HPSCS_FAMILY_ID ? HPSCS_MRU_PLATFORM_ID_CHAR_1 : HP_SMART_CARD_INK_MRU_PLATFORM_ID_CHAR_1);
        result  = LIB_HP_SMART_CARD_write_string(component_id,
                                                 field,
                                                 12,
                                                 "Test Value {");
        if (check_result(result, ">>> Failed to write MRU Platform ID (2).  "))
            return -1;
        HP_USLEEP(1000000);                 // 1 second delay
        result = LIB_HP_SMART_CARD_flush();
        if (check_result(result, ">>> Failed to flush (2).  "))
            return -1;


        // @@@ Read MRU Platform ID to note string changes @@@

        result = LIB_HP_SMART_CARD_read_string(component_id,
                                               field,
                                               12,
                                               buffer);
        buffer[12] = '\0';
        if (check_result(result, ">>> Failed to read MRU Platform ID.  "))
            return -1;
        else
            printf(">>> MRU Platform ID = \"%s\"\n", buffer);
    }

    // ----- END Loop -----


    // @@@ Shut down the library @@@

    LIB_HP_SMART_CARD_shutdown();
    printf(">>> LIB_HP_SMART_CARD_shutdown() complete\n");

    return 0;
}
