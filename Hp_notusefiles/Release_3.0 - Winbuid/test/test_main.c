/*
   Confidential computer software. Valid license from HP required for possession, use or copying.  Consistent with FAR 12.211 and 12.212, Commercial Computer Software, Computer Software Documentation, and Technical Data for Commercial Items are licensed to the U.S. Government under vendor's standard commercial license.

   THE LICENSED SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY DESCRIPTION.  HP SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  YOU ASSUME THE ENTIRE RISK RELATING TO THE USE OR PERFORMANCE OF THE LICENSED SOFTWARE.

   HP Company Confidential
   © Copyright 2009-2015 HP Development Company, L.P.
   Made in U.S.A.
 */

// for Windows, removes warning about use of scanf, sscanf
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

#include "hp_smart_card.h"
#include "hp_smart_card_gpio_ifc.h"
#include "hp_smart_card_i2c_ifc.h"
#include "hp_debug_log_internal.h"
#include "hp_assert.h"
#include "hp_generic_macros.h"

#include "hp_host_smart_card_ifc.h"
#include "hp_host_smart_card.h"


/**************************************
 * Typedefs
 *************************************/
typedef void (*test_case_func)(void);

typedef struct
{
    char            *test_name;
    test_case_func  test_func_ptr;
} test_case_t;

/**************************************
 * Forward decalrations
 *************************************/
static void _analyze_menu(void);
static void _display_menu(void);

static void _get_library_version(void);
static void _set_log_depth(void);

static void _host_smart_card_echo(void);
static void _smart_card_echo(void);
static void _device_init(void);
static void _device_close(void);

static void _read_family_id(void);
static void _read_mfg_year(void);
static void _read_ctrdg_mru_year(void);
static void _write_ctrdg_mru_year(void);

static void _read_ooi_bit(void);
static void _set_ooi_bit(void);
static void _clear_ooi_bit(void);

static void _read_first_install_year(void);
static void _write_first_install_year(void);
static void _read_wp_fuse(void);
static void _set_wp_fuse(void);
static void _clear_wp_fuse(void);
static void _first_inst_change_part(void);

static void _read_reorder_part_num(void);

static void _read_first_platform_id(void);
static void _write_first_platform_id(void);

static void _library_shutdown(void);

/**************************************
 * Static variables
 *************************************/
static test_case_t test_case[] = {
    { "Get Library Version",                _get_library_version      },
    { "Set Log Depth",                      _set_log_depth            },

    { "Host smart card Echo",               _host_smart_card_echo     },
    { "Smart card Echo",                    _smart_card_echo          },
    { "DEVICE INIT",                        _device_init              },
    { "Device close",                       _device_close             },

    { "Read Family ID",                     _read_family_id           },
    { "Read manufacture year",              _read_mfg_year            },
    { "Read MRU year",                      _read_ctrdg_mru_year      },
    { "Write MRU year",                     _write_ctrdg_mru_year     },

    { "Read OOI bit",                       _read_ooi_bit             },
    { "Set OOI bit",                        _set_ooi_bit              },
    { "Clear OOI bit",                      _clear_ooi_bit            },

    { "Read First Install year",            _read_first_install_year  },
    { "Write First Install year",           _write_first_install_year },
    { "Read First Install partition fuse",  _read_wp_fuse             },
    { "Set First Install partition fuse",   _set_wp_fuse              },
    { "Clear First Install partition fuse", _clear_wp_fuse            },
    { "First Install partition to RO",      _first_inst_change_part   },

    { "Read Reorder Part num",              _read_reorder_part_num    },

    { "Read First Platform ID",             _read_first_platform_id   },
    { "Write First Platform ID",            _write_first_platform_id  },

    { "Library Shutdown",                   _library_shutdown         },
};

#define NUM_TEST_CASES      (sizeof(test_case) / sizeof(test_case_t))
#define MENU_COL_GAP        3

static int  menu_col_width;
static int  menu_col2_offset;

#include "carnac_types.h"


void print_returns(HP_SMART_CARD_result_t result)
{
    HW_SMART_CARD_status_t status = LIB_HP_SMART_CARD_last_status();
    printf("Result = %s (%d)  Status = %s (%d)\n",
           LIB_HP_SMART_CARD_result_string(result), result,
           LIB_HP_SMART_CARD_status_string(status), status);
}

void assert_handler(const char *error_str)
{
    printf("=========================================\n");
    printf("Test Main: HP_ASSERT Failed\n");
    printf("%s\n", error_str);
    printf("=========================================\n");
}

void cache_monitor_failure_handler(HP_SMART_CARD_device_id_t dev_id,
                                   HP_SMART_CARD_result_t result)
{
    printf("=========================================\n");
    printf("Test Main: Cache monitor failure\n");
    printf("Device Id = %d, ", dev_id);
    print_returns(result);
    printf("=========================================\n");
}

int main()
{
    int     user_input;
    char    user_buf[100];
    int     fields_read;

    // Initialise low level libraries. GPIO and I2C
    HP_SMART_CARD_gpio_init();
    HP_SMART_CARD_i2c_init();

    printf("Initializing smart card library....\n");

    // Register for assert callback
    LIB_HP_SMART_CARD_register_assert_callback(assert_handler);

    // Register for cache monitor callack
    LIB_HP_SMART_CARD_register_cache_monitor_callback(cache_monitor_failure_handler);

    // Initialise the library
    LIB_HP_SMART_CARD_init();

    printf("Initializing smart card library....Done.\n");

    // decide formatting information
    _analyze_menu();

    while (1)
    {
        // Disaply the available test cases to user
        _display_menu();

        // Wait for the user input
        scanf("%s", (char *) &user_buf);

        // exit on 'e'
        if (user_buf[0] == 'e' || user_buf[0] == 'E')
            break;

        // convert string to integer value
        fields_read = sscanf(user_buf, "%d", &user_input);

        // check for invalid test number
        if (fields_read != 1 || user_input < 0 || user_input >= NUM_TEST_CASES)
        {
            printf("Invalid entry. Enter 'e' to exit.\n");
            continue;
        }

        // invoke appropriate test function
        test_case[user_input].test_func_ptr();
    }

    return 0;
}

static void _analyze_menu(void)
{
    int i, w;
    menu_col_width = 0;

    menu_col2_offset = (NUM_TEST_CASES + 1) / 2;
    for (i = 0; i < menu_col2_offset; i++)
    {
        w = strlen(test_case[i].test_name);
        if (w > menu_col_width)
            menu_col_width = w;
    }
}

static void _display_menu(void)
{
    int i, w;

    printf("\n");

    for (i = 0; i < menu_col2_offset; i++)
    {
        w = strlen(test_case[i].test_name);
        printf("%2d %-*s", i, (menu_col_width + MENU_COL_GAP), test_case[i].test_name);
        if (i + menu_col2_offset < NUM_TEST_CASES)
            printf("%2d %s\n", (i + menu_col2_offset), test_case[i + menu_col2_offset].test_name);
        else
            printf("\n");
    }

    printf("Test number ('e' to exit)? ");
}


static void _get_library_version(void)
{
    printf("HP SMART CARD Library version = %s\n", LIB_HP_SMART_CARD_get_version());
    return;
}


static void _set_log_depth(void)
{
    unsigned int user_input;

    printf("Log depth (0-5)? ");
    scanf("%d", &user_input);

    LIB_HP_SMART_CARD_set_log_depth((unsigned char) user_input);
}


static void _host_smart_card_echo(void)
{
    HP_SMART_CARD_result_t result;
    result = LIB_HP_SMART_CARD_device_present(HP_SMART_CARD_DEVICE_HOST);
    printf("LIB_HP_SMART_CARD_device_present(): Result = %d\n", result);

    return;
}
static void _smart_card_echo(void)
{
    HP_SMART_CARD_result_t result;
    result = LIB_HP_SMART_CARD_device_present(HP_SMART_CARD_DEVICE_ID_0);
    printf("LIB_HP_SMART_CARD_device_present(): Result = %d\n", result);

    return;
}


static void _device_init(void)
{
    HP_SMART_CARD_result_t result;

    result = LIB_HP_SMART_CARD_device_init(HP_SMART_CARD_DEVICE_ID_0);

    if (HP_SMART_CARD_OK == result)
    {
        printf("Intialized HP_SMART_CARD_DEVICE_ID_0\n");
    }
    else
    {
        printf("Failed to initialize HP_SMART_CARD_DEVICE_ID_0.  ");
        print_returns(result);
    }
}


static void _device_close(void)
{
    HP_SMART_CARD_result_t result;

    result = LIB_HP_SMART_CARD_device_close(HP_SMART_CARD_DEVICE_ID_0);

    if (HP_SMART_CARD_OK == result)
    {
        printf("Closed HP_SMART_CARD_DEVICE_ID_0\n");
    }
    else
    {
        printf("Failed to close HP_SMART_CARD_DEVICE_ID_0.  ");
        print_returns(result);
    }
}


static void _read_family_id(void)
{
    HP_SMART_CARD_result_t  result;
    uint8_t                 family_id;

    result = LIB_HP_SMART_CARD_read_field(HP_SMART_CARD_DEVICE_ID_0,
                                          HP_SMART_CARD_FAMILY_ID,
                                          sizeof(family_id),
                                          &family_id);

    if (HP_SMART_CARD_OK == result)
    {
        printf("Family Id = %d\n", family_id);
    }
    else
    {
        printf("Failed to read Family Id.  ");
        print_returns(result);
    }
}


static void _read_mfg_year(void)
{
    HP_SMART_CARD_result_t  result;
    uint8_t                 mfg_year;

    result = LIB_HP_SMART_CARD_read_field(HP_SMART_CARD_DEVICE_ID_0, HP_SMART_CARD_INK_MANUFACTURE_YEAR, 1, &mfg_year);

    if (HP_SMART_CARD_OK == result)
    {
        printf("Mfg year = %d\n", mfg_year + 2006);
    }
    else
    {
        printf("Failed to read MFG Year.  ");
        print_returns(result);
    }
}


static void _read_ctrdg_mru_year(void)
{
    HP_SMART_CARD_result_t  result;
    uint8_t                 mfg_year;

    result = LIB_HP_SMART_CARD_read_field(HP_SMART_CARD_DEVICE_ID_0,
                                          HP_SMART_CARD_INK_CARTRIDGE_MRU_YEAR,
                                          1,
                                          &mfg_year);

    if (HP_SMART_CARD_OK == result)
    {
        printf("MRU year = %d\n", mfg_year + 2006);
    }
    else
    {
        printf("Failed to read MRU Year.  ");
        print_returns(result);
    }
}
static void _write_ctrdg_mru_year(void)
{
    HP_SMART_CARD_result_t  result;
    uint32_t                mru_year;

    printf("MRU year (>=2006)? ");
    scanf("%d", &mru_year);
    mru_year -= 2006;

    result = LIB_HP_SMART_CARD_write_field(HP_SMART_CARD_DEVICE_ID_0,
                                           HP_SMART_CARD_INK_CARTRIDGE_MRU_YEAR,
                                           4,
                                           &mru_year);

    if (HP_SMART_CARD_OK == result)
    {
        printf("MRU year = %d\n", mru_year + 2006);
    }
    else
    {
        printf("Failed to write MRU Year.  ");
        print_returns(result);
    }
}


static void _read_ooi_bit(void)
{
    HP_SMART_CARD_result_t  result;
    uint8_t                 ooi_bit;

    result = LIB_HP_SMART_CARD_read_field(HP_SMART_CARD_DEVICE_ID_0,
                                          HP_SMART_CARD_INK_OUT_OF_INK_BIT,
                                          1,
                                          &ooi_bit);
    if (HP_SMART_CARD_OK == result)
    {
        printf("OOI bit (OR partition) = %d\n", ooi_bit);
    }
    else
    {
        printf("Failed to read ooi_bit.  ");
        print_returns(result);
    }
}
static void _set_ooi_bit(void)
{
    HP_SMART_CARD_result_t  result;
    uint8_t                 ooi_bit = 1;

    result = LIB_HP_SMART_CARD_write_field(HP_SMART_CARD_DEVICE_ID_0,
                                           HP_SMART_CARD_INK_OUT_OF_INK_BIT,
                                           1,
                                           &ooi_bit);
    if (HP_SMART_CARD_OK == result)
    {
        printf("OOI bit (OR partition) SET\n");
    }
    else
    {
        printf("Failed to write ooi_bit (OR partition).  ");
        print_returns(result);
    }
}
static void _clear_ooi_bit(void)
{
    HP_SMART_CARD_result_t  result;
    uint8_t                 ooi_bit = 0;

    result = LIB_HP_SMART_CARD_write_field(HP_SMART_CARD_DEVICE_ID_0,
                                           HP_SMART_CARD_INK_OUT_OF_INK_BIT,
                                           1,
                                           &ooi_bit);
    if (HP_SMART_CARD_OK == result)
    {
        printf("OOI bit (OR partition) CLEAR\n");
    }
    else
    {
        printf("Failed to write ooi_bit (OR partition).  ");
        print_returns(result);
    }
}


static void _read_first_install_year(void)
{
    HP_SMART_CARD_result_t  result;
    uint8_t                 fi_year;

    result = LIB_HP_SMART_CARD_read_field(HP_SMART_CARD_DEVICE_ID_0,
                                          HP_SMART_CARD_INK_FIRST_PLATFORM_MFG_YEAR,
                                          1,
                                          &fi_year);

    if (HP_SMART_CARD_OK == result)
    {
        printf("First Install year = %d\n", fi_year + 2006);
    }
    else
    {
        printf("Failed to read First Install Year.  ");
        print_returns(result);
    }
}
static void _write_first_install_year(void)
{
    HP_SMART_CARD_result_t  result;
    uint32_t                fi_year;

    printf("First Install year (>=2006)? ");
    scanf("%d", &fi_year);
    fi_year -= 2006;

    result = LIB_HP_SMART_CARD_write_field(HP_SMART_CARD_DEVICE_ID_0,
                                           HP_SMART_CARD_INK_FIRST_PLATFORM_MFG_YEAR,
                                           4,
                                           &fi_year);

    if (HP_SMART_CARD_OK == result)
    {
        printf("First Install year = %d\n", fi_year + 2006);
    }
    else
    {
        printf("Failed to write First Install Year.  ");
        print_returns(result);
    }
}
static void _read_wp_fuse(void)
{
    HP_SMART_CARD_result_t  result;
    uint8_t                 fuse;

    result = LIB_HP_SMART_CARD_read_field(HP_SMART_CARD_DEVICE_ID_0,
                                          HP_SMART_CARD_INK_WRITE_PROTECT,
                                          1,
                                          &fuse);
    if (HP_SMART_CARD_OK == result)
    {
        printf("First Install WP Fuse = %d\n", fuse);
    }
    else
    {
        printf("Failed to read First Install WP Fuse. Error = %d\n", result);
    }
}
static void _set_wp_fuse(void)
{
    HP_SMART_CARD_result_t  result;
    uint8_t                 fuse = 1;

    result = LIB_HP_SMART_CARD_write_field(HP_SMART_CARD_DEVICE_ID_0,
                                           HP_SMART_CARD_INK_WRITE_PROTECT,
                                           1,
                                           &fuse);
    if (HP_SMART_CARD_OK == result)
    {
        printf("First Install WP Fuse SET (does NOT change parition to RO).\n");
    }
    else
    {
        printf("Failed to write First Install WP Fuse. Error = %d\n", result);
    }
}
static void _clear_wp_fuse(void)
{
    HP_SMART_CARD_result_t  result;
    uint8_t                 fuse = 0;

    result = LIB_HP_SMART_CARD_write_field(HP_SMART_CARD_DEVICE_ID_0,
                                           HP_SMART_CARD_INK_WRITE_PROTECT,
                                           1,
                                           &fuse);
    if (HP_SMART_CARD_OK == result)
    {
        printf("First Install WP Fuse CLEARED.\n");
    }
    else
    {
        printf("Failed to write First Install WP Fuse. Error = %d\n", result);
    }
}
static void _first_inst_change_part(void)
{
    HP_SMART_CARD_result_t  result;
    const int               partition = 2;

    result = LIB_HP_SMART_CARD_change_RW_partition_to_RO(HP_SMART_CARD_DEVICE_ID_0, (uint8_t) partition);
    if (HP_SMART_CARD_OK == result)
    {
        printf("First Install Partition (%d) changed to RO.\n", partition);
    }
    else
    {
        printf("Failed to change partition to RO.  ");
        print_returns(result);
    }
}


static void _read_reorder_part_num(void)
{
    HP_SMART_CARD_result_t  result;
    uint8_t                 reorder_part_num[12];
    int                     i = 0;

    // Changed Here
    for (i = 0; i < 12; i++)
    {
        result = LIB_HP_SMART_CARD_read_field(HP_SMART_CARD_DEVICE_ID_0,
                                              HP_SMART_CARD_INK_REORDER_PART_NUM_CHAR_1 + i,
                                              sizeof(reorder_part_num[i]),
                                              &reorder_part_num[i]);
        if (HP_SMART_CARD_OK == result)
            continue;
        else
            break;
    }


    if (HP_SMART_CARD_OK == result)
    {
        printf("Reorder Part Num = ");
        for (i = 0; i < 12; i++)
        {
            printf("%c ", (reorder_part_num[i]) + 32);
        }
    }
    else
    {
        printf("Failed to read Re-order platform ID.  ");
        print_returns(result);
    }
}


static void _read_first_platform_id(void)
{
    HP_SMART_CARD_result_t  result;
    uint8_t                 first_platform_id[13];

    // read as a string
    result = LIB_HP_SMART_CARD_read_string(HP_SMART_CARD_DEVICE_ID_0,
                                           HP_SMART_CARD_INK_MRU_PLATFORM_ID_CHAR_1,
                                           12,
                                           first_platform_id);
    first_platform_id[12] = '\0';
    if (HP_SMART_CARD_OK == result)
    {
        printf("first_platform_id = \"%s\"\n", first_platform_id);
    }
    else
    {
        printf("Failed to read first_platform_id.  ");
        print_returns(result);
    }
}
static void _write_first_platform_id(void)
{
    HP_SMART_CARD_result_t  result;
    uint8_t                 first_platform_id[13] = "Test Value {";

    // write as string
    result = LIB_HP_SMART_CARD_write_string(HP_SMART_CARD_DEVICE_ID_0,
                                            HP_SMART_CARD_INK_MRU_PLATFORM_ID_CHAR_1,
                                            12,
                                            first_platform_id);
    if (HP_SMART_CARD_OK == result)
    {
        printf("Success! The first_platform_id was written as \"%s\".", first_platform_id);
    }
    else
    {
        printf("Failed to write first_platform_id.  ");
        print_returns(result);
    }
}


static void _library_shutdown(void)
{
    LIB_HP_SMART_CARD_shutdown();

    printf("Completed LIB_HP_SMART_CARD_shutdown()\n");
}

