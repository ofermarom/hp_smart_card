/*
   Confidential computer software. Valid license from HP required for possession, use or copying.  Consistent with FAR 12.211 and 12.212, Commercial Computer Software, Computer Software Documentation, and Technical Data for Commercial Items are licensed to the U.S. Government under vendor's standard commercial license.

   THE LICENSED SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY DESCRIPTION.  HP SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  YOU ASSUME THE ENTIRE RISK RELATING TO THE USE OR PERFORMANCE OF THE LICENSED SOFTWARE.

   HP Company Confidential
   © Copyright 2009-2015 HP Development Company, L.P.
   Made in U.S.A.
 */

//*****************************************************************************
// File : hp_smart_card_i2c.c
//-----------------------------------------------------------------------------
// Description: Fake I2C driver for board_dummy.
//
//*****************************************************************************

#include <stdio.h>

#include "hp_smart_card_config.h"
#include "hp_smart_card_i2c_ifc.h"

/***********************************************
* Implementation of APIs
***********************************************/
void HP_SMART_CARD_i2c_init(void)
{
    printf("Initialized fake I2C driver\n");
    return;
}

HP_SMART_CARD_i2c_result_t HP_SMART_CARD_i2c_read(HP_SMART_CARD_device_id_t device_id,
                                                  uint8_t addr,
                                                  uint8_t                   *data,
                                                  size_t num_bytes_to_read)
{
    printf("Fake I2C driver: HP_SMART_CARD_i2c_read()\n");
    return HP_SMART_CARD_I2C_SUCCESS;
}

HP_SMART_CARD_i2c_result_t HP_SMART_CARD_i2c_write(HP_SMART_CARD_device_id_t device_id,
                                                   uint8_t addr,
                                                   uint8_t                   *data,
                                                   size_t num_bytes_to_write)
{
    printf("Fake I2C driver: HP_SMART_CARD_i2c_write()\n");
    return HP_SMART_CARD_I2C_SUCCESS;
}
