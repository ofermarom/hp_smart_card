/*
   Confidential computer software. Valid license from HP required for possession, use or copying.  Consistent with FAR 12.211 and 12.212, Commercial Computer Software, Computer Software Documentation, and Technical Data for Commercial Items are licensed to the U.S. Government under vendor's standard commercial license.

   THE LICENSED SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY DESCRIPTION.  HP SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  YOU ASSUME THE ENTIRE RISK RELATING TO THE USE OR PERFORMANCE OF THE LICENSED SOFTWARE.

   HP Company Confidential
   � Copyright 2009-2016 HP Development Company, L.P.
   Made in U.S.A.
 */

#include <stdio.h>

#include "hp_smart_card.h"
#include "hp_os_service.h"

#include "hp_smart_card_gpio_ifc.h"
#include "hp_smart_card_i2c_ifc.h"


void print_returns(int result)
{
    HW_SMART_CARD_status_t status = LIB_HP_SMART_CARD_last_status();
    if (HW_SMART_CARD_success_e == status)
        printf("Result = %s (%d)\n",
               LIB_HP_SMART_CARD_result_string(result), result);
    else
        printf("Result = %s (%d)  Status = %s (%d)\n",
               LIB_HP_SMART_CARD_result_string(result), result,
               LIB_HP_SMART_CARD_status_string(status), status);
}


void assert_handler(const char *error_str)
{
    printf("=========================================\n");
    printf("Test Main: HP_ASSERT Failed\n");
    printf("%s\n", error_str);
    printf("=========================================\n");
}

void cache_monitor_failure_handler(HP_SMART_CARD_device_id_t dev_id,
                                   HP_SMART_CARD_result_t result)
{
    printf("=========================================\n");
    printf("Test Main: Cache monitor failure\n");
    printf("Device Id = %d, ", dev_id);
    print_returns(result);
    printf("=========================================\n");
}


int check_result(int result, char* msg)
{
    if ((int) HP_SMART_CARD_OK == result)
        return 0;
    printf(msg);
    print_returns(result);
    return -1;
}



int main()
{
    HP_SMART_CARD_result_t  result;
    unsigned char           family_id;
    uint32_t                u32;
    char                    buffer[31];
	uint8_t                 mfg_year;

      setbuf(stdout, NULL); 
    // Set logging level
    LIB_HP_SMART_CARD_set_log_depth(0);

    // Initialize GPIO and I2C drivers
    HP_SMART_CARD_gpio_init();
    HP_SMART_CARD_i2c_init();

    // Initialise the library
    printf(">>> HP SMART CARD Library version = %s\n", LIB_HP_SMART_CARD_get_version());
    LIB_HP_SMART_CARD_init();

    // Un-comment the next line to show CUSTOMER logging
    //LIB_HP_SMART_CARD_set_log_mask(HP_DBG_LEVEL_CUSTOMER);

    // Register for assert callback
    LIB_HP_SMART_CARD_register_assert_callback(assert_handler);

    // Register for cache monitor callack
    LIB_HP_SMART_CARD_register_cache_monitor_callback(cache_monitor_failure_handler);


    // @@@ Check for presence of Component and Host smart cards @@@

    result = LIB_HP_SMART_CARD_device_present(HP_SMART_CARD_DEVICE_ID_0);
    if (check_result(result, ">>> LIB_HP_SMART_CARD_device_present(HP_SMART_CARD_DEVICE_ID_0): NOT PRESENT.  "))
        return -1;
    else
        printf(">>> LIB_HP_SMART_CARD_device_present(HP_SMART_CARD_DEVICE_ID_0): Present\n");

    result = LIB_HP_SMART_CARD_device_present(HP_SMART_CARD_DEVICE_HOST);
    if (check_result(result, ">>> LIB_HP_SMART_CARD_device_present(HP_SMART_CARD_DEVICE_HOST): NOT PRESENT.  "))
        return -1;
    else
        printf(">>> LIB_HP_SMART_CARD_device_present(HP_SMART_CARD_DEVICE_HOST): Present\n");


    // @@@ Initialize the Component smart card @@@

    result = LIB_HP_SMART_CARD_device_init(HP_SMART_CARD_DEVICE_ID_0);
    if (check_result(result, ">>> LIB_HP_SMART_CARD_device_init(HP_SMART_CARD_DEVICE_ID_0).  "))
        return -1;
    else
        printf(">>> LIB_HP_SMART_CARD_device_init(HP_SMART_CARD_DEVICE_ID_0): Successful\n");


    // @@@ Read the Family ID @@@

    result = LIB_HP_SMART_CARD_read_field(HP_SMART_CARD_DEVICE_ID_0,
                                          HP_SMART_CARD_FAMILY_ID,
                                          sizeof(family_id),
                                          &family_id);
    if (check_result(result, ">>> Failed to read Family ID.  "))
        return -1;
    else
        printf(">>> Family ID = %d\n", family_id);
    // validate the Family ID
#ifdef INCLUDE_HP_SMART_CARD_SUPPLY
    if (family_id == HPSCS_FAMILY_ID)
    {
        printf(">>> WARNING: This test program is not intended for Family ID %d (HPSCS_FAMILY_ID).\n", HPSCS_FAMILY_ID);
        goto shutdown;
    }
#endif
    if (family_id != HP_SMART_CARD_INK_FAMILY_ID)
    {
        printf(">>> ERROR: Family ID %d is expected.\n", HP_SMART_CARD_INK_FAMILY_ID);
        return -1;
    }

    // @@@ Write and flush MRU Platform ID @@@

    result = LIB_HP_SMART_CARD_write_string(HP_SMART_CARD_DEVICE_ID_0,
                                            (HP_SMART_CARD_field_t) HP_SMART_CARD_INK_MRU_PLATFORM_ID_CHAR_1,
                                            12,
                                            "abcdefgh1234");
    if (check_result(result, ">>> Failed to write MRU Platform ID.  "))
        return -1;
    HP_USLEEP(3000000);                 // 3 second delay
    result = LIB_HP_SMART_CARD_flush();
    if (check_result(result, ">>> Failed to flush.  "))
        return -1;


    
    // @@@ Read, increment, and write the OEM Defined RW Field 1 @@@

    result = LIB_HP_SMART_CARD_read_field(HP_SMART_CARD_DEVICE_ID_0,
                                          (HP_SMART_CARD_field_t) HP_SMART_CARD_INK_OEM_DEF_RW_FIELD_1,
                                          sizeof(u32),
                                          &u32);
    if (check_result(result, ">>> Failed to read OEM Defined RW Field 1.  "))
        return -1;
    u32++;      // increment
    result = LIB_HP_SMART_CARD_write_field(HP_SMART_CARD_DEVICE_ID_0,
                                           (HP_SMART_CARD_field_t) HP_SMART_CARD_INK_OEM_DEF_RW_FIELD_1,
                                           sizeof(u32),
                                           &u32);
    if (check_result(result, ">>> Failed to read OEM Defined RW Field 1.  "))
        return -1;
    else
        printf(">>> OEM Defined RW Field 1 incremented = %d\n", u32);
    // NOTE: data has not been flushed (single-threaded)


    // @@@ Write and flush MRU Platform ID a second time @@@

    result = LIB_HP_SMART_CARD_write_string(HP_SMART_CARD_DEVICE_ID_0,
                                            (HP_SMART_CARD_field_t) HP_SMART_CARD_INK_MRU_PLATFORM_ID_CHAR_1,
                                            12,
                                            "Test Value {");
    if (check_result(result, ">>> Failed to write MRU Platform ID (2).  "))
        return -1;
    HP_USLEEP(3000000);                 // 3 second delay
    result = LIB_HP_SMART_CARD_flush();
    if (check_result(result, ">>> Failed to flush (2).  "))
        return -1;


    // @@@ Read MRU Platform ID to note string changes @@@

    result = LIB_HP_SMART_CARD_read_string(HP_SMART_CARD_DEVICE_ID_0,
                                           (HP_SMART_CARD_field_t) HP_SMART_CARD_INK_MRU_PLATFORM_ID_CHAR_1,
                                           12,
                                           buffer);
    buffer[12] = '\0';
    if (check_result(result, ">>> Failed to read MRU Platform ID.  "))
        return -1;
    else
        printf(">>> MRU Platform ID = \"%s\"\n", buffer);

	// @@@ Write and flush Manufacture ID @@@
	mfg_year = 0;
	/*printf(">>> Writing Manufacture ID = \"%d\"\n", mfg_year);
	result = LIB_HP_SMART_CARD_write_field(HP_SMART_CARD_DEVICE_ID_0,
		(HP_SMART_CARD_field_t)HP_SMART_CARD_INK_INK_FAMILY_MEMBER,
		1,
		&mfg_year);
	if (check_result(result, ">>> Failed to write Manufacture ID.  "))
		return -1;
	HP_USLEEP(3000000);                 // 3 second delay
	result = LIB_HP_SMART_CARD_flush();
	if (check_result(result, ">>> Failed to flush (2).  "))
		return -1;*/


	// @@@ Read Manufacture ID to note string changes @@@

	result = LIB_HP_SMART_CARD_read_field(HP_SMART_CARD_DEVICE_ID_0,
		(HP_SMART_CARD_field_t)HP_SMART_CARD_INK_INK_REVISION,
		1,
		&mfg_year);

	if (check_result(result, ">>> Failed to read Manufacture ID.  "))
		return -1;
	else
		printf(">>> Manufacture ID = \"%02x\"\n", mfg_year);


    // @@@ Shut down the library @@@

#ifdef INCLUDE_HP_SMART_CARD_SUPPLY
 shutdown:
#endif
    LIB_HP_SMART_CARD_shutdown();
    printf(">>> LIB_HP_SMART_CARD_shutdown() complete\n");

    return 0;
}
