#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/drivers/board_raspberrypi/hp_smart_card_gpio.o \
	${OBJECTDIR}/drivers/board_raspberrypi/hp_smart_card_i2c.o \
	${OBJECTDIR}/infrastructure/assert/hp_assert.o \
	${OBJECTDIR}/infrastructure/debug_log/hp_debug_log.o \
	${OBJECTDIR}/infrastructure/debug_log/hp_utils.o \
	${OBJECTDIR}/os/os_posix/hp_os_services.o \
	${OBJECTDIR}/os/os_posix/msgq.o \
	${OBJECTDIR}/src/crypto/crypto_aes.o \
	${OBJECTDIR}/src/crypto/crypto_cmac.o \
	${OBJECTDIR}/src/hp_host_smart_card/hp_host_smart_card.o \
	${OBJECTDIR}/src/hp_host_smart_card/hp_host_smart_card_chip.o \
	${OBJECTDIR}/src/hp_host_smart_card/hp_host_smart_card_xpt_i2c.o \
	${OBJECTDIR}/src/hp_smart_card/hp_smart_card.o \
	${OBJECTDIR}/src/hp_smart_card/hw_smart_card.o \
	${OBJECTDIR}/src/hp_smart_card/hw_smart_card_proto.o \
	${OBJECTDIR}/src/hp_smart_card/hw_smart_card_xpt_i2c.o \
	${OBJECTDIR}/src/hp_smart_card/lib_crc.o \
	${OBJECTDIR}/test/test_program.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lbcm2835 -lpthread

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ver3_0netbeans

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ver3_0netbeans: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/ver3_0netbeans ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/drivers/board_raspberrypi/hp_smart_card_gpio.o: drivers/board_raspberrypi/hp_smart_card_gpio.c
	${MKDIR} -p ${OBJECTDIR}/drivers/board_raspberrypi
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/drivers/board_raspberrypi/hp_smart_card_gpio.o drivers/board_raspberrypi/hp_smart_card_gpio.c

${OBJECTDIR}/drivers/board_raspberrypi/hp_smart_card_i2c.o: drivers/board_raspberrypi/hp_smart_card_i2c.c
	${MKDIR} -p ${OBJECTDIR}/drivers/board_raspberrypi
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/drivers/board_raspberrypi/hp_smart_card_i2c.o drivers/board_raspberrypi/hp_smart_card_i2c.c

${OBJECTDIR}/infrastructure/assert/hp_assert.o: infrastructure/assert/hp_assert.c
	${MKDIR} -p ${OBJECTDIR}/infrastructure/assert
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/infrastructure/assert/hp_assert.o infrastructure/assert/hp_assert.c

${OBJECTDIR}/infrastructure/debug_log/hp_debug_log.o: infrastructure/debug_log/hp_debug_log.c
	${MKDIR} -p ${OBJECTDIR}/infrastructure/debug_log
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/infrastructure/debug_log/hp_debug_log.o infrastructure/debug_log/hp_debug_log.c

${OBJECTDIR}/infrastructure/debug_log/hp_utils.o: infrastructure/debug_log/hp_utils.c
	${MKDIR} -p ${OBJECTDIR}/infrastructure/debug_log
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/infrastructure/debug_log/hp_utils.o infrastructure/debug_log/hp_utils.c

${OBJECTDIR}/os/os_posix/hp_os_services.o: os/os_posix/hp_os_services.c
	${MKDIR} -p ${OBJECTDIR}/os/os_posix
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/os/os_posix/hp_os_services.o os/os_posix/hp_os_services.c

${OBJECTDIR}/os/os_posix/msgq.o: os/os_posix/msgq.c
	${MKDIR} -p ${OBJECTDIR}/os/os_posix
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/os/os_posix/msgq.o os/os_posix/msgq.c

${OBJECTDIR}/src/crypto/crypto_aes.o: src/crypto/crypto_aes.c
	${MKDIR} -p ${OBJECTDIR}/src/crypto
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/crypto/crypto_aes.o src/crypto/crypto_aes.c

${OBJECTDIR}/src/crypto/crypto_cmac.o: src/crypto/crypto_cmac.c
	${MKDIR} -p ${OBJECTDIR}/src/crypto
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/crypto/crypto_cmac.o src/crypto/crypto_cmac.c

${OBJECTDIR}/src/hp_host_smart_card/hp_host_smart_card.o: src/hp_host_smart_card/hp_host_smart_card.c
	${MKDIR} -p ${OBJECTDIR}/src/hp_host_smart_card
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/hp_host_smart_card/hp_host_smart_card.o src/hp_host_smart_card/hp_host_smart_card.c

${OBJECTDIR}/src/hp_host_smart_card/hp_host_smart_card_chip.o: src/hp_host_smart_card/hp_host_smart_card_chip.c
	${MKDIR} -p ${OBJECTDIR}/src/hp_host_smart_card
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/hp_host_smart_card/hp_host_smart_card_chip.o src/hp_host_smart_card/hp_host_smart_card_chip.c

${OBJECTDIR}/src/hp_host_smart_card/hp_host_smart_card_xpt_i2c.o: src/hp_host_smart_card/hp_host_smart_card_xpt_i2c.c
	${MKDIR} -p ${OBJECTDIR}/src/hp_host_smart_card
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/hp_host_smart_card/hp_host_smart_card_xpt_i2c.o src/hp_host_smart_card/hp_host_smart_card_xpt_i2c.c

${OBJECTDIR}/src/hp_smart_card/hp_smart_card.o: src/hp_smart_card/hp_smart_card.c
	${MKDIR} -p ${OBJECTDIR}/src/hp_smart_card
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/hp_smart_card/hp_smart_card.o src/hp_smart_card/hp_smart_card.c

${OBJECTDIR}/src/hp_smart_card/hw_smart_card.o: src/hp_smart_card/hw_smart_card.c
	${MKDIR} -p ${OBJECTDIR}/src/hp_smart_card
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/hp_smart_card/hw_smart_card.o src/hp_smart_card/hw_smart_card.c

${OBJECTDIR}/src/hp_smart_card/hw_smart_card_proto.o: src/hp_smart_card/hw_smart_card_proto.c
	${MKDIR} -p ${OBJECTDIR}/src/hp_smart_card
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/hp_smart_card/hw_smart_card_proto.o src/hp_smart_card/hw_smart_card_proto.c

${OBJECTDIR}/src/hp_smart_card/hw_smart_card_xpt_i2c.o: src/hp_smart_card/hw_smart_card_xpt_i2c.c
	${MKDIR} -p ${OBJECTDIR}/src/hp_smart_card
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/hp_smart_card/hw_smart_card_xpt_i2c.o src/hp_smart_card/hw_smart_card_xpt_i2c.c

${OBJECTDIR}/src/hp_smart_card/lib_crc.o: src/hp_smart_card/lib_crc.c
	${MKDIR} -p ${OBJECTDIR}/src/hp_smart_card
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/hp_smart_card/lib_crc.o src/hp_smart_card/lib_crc.c

${OBJECTDIR}/test/test_program.o: test/test_program.c
	${MKDIR} -p ${OBJECTDIR}/test
	${RM} "$@.d"
	$(COMPILE.c) -g -Iinfrastructure/internal_ifc -Iproduct/tij25 -Idrivers -Iifc -Iinfrastructure -Inbproject -Ios -Iproduct -Isrc -Itest -Idrivers/board_dummy -Idrivers/board_raspberrypi -Idrivers/internal_ifc -Iinfrastructure/assert -Iinfrastructure/debug_log -Ios/internal_ifc -Ios/os_posix -Ios/os_raspberrypi -Ios/os_uclinux -Ios/os_windows -Isrc/crypto -Isrc/hp_host_smart_card -Isrc/hp_smart_card -Isrc/internal_ifc -Itest -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/test/test_program.o test/test_program.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
