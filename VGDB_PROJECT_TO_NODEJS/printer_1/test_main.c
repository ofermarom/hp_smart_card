/*
   Confidential computer software. Valid license from HP required for possession, use or copying.  Consistent with FAR 12.211 and 12.212, Commercial Computer Software, Computer Software Documentation, and Technical Data for Commercial Items are licensed to the U.S. Government under vendor's standard commercial license.

   THE LICENSED SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY DESCRIPTION.  HP SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  YOU ASSUME THE ENTIRE RISK RELATING TO THE USE OR PERFORMANCE OF THE LICENSED SOFTWARE.

   HP Company Confidential
   © Copyright 2009-2015 HP Development Company, L.P.
   Made in U.S.A.
 */

 // for Windows, removes warning about use of scanf, sscanf
#define _CRT_SECURE_NO_WARNINGS
#define VERSION_LIB_INTERFACE

#include <stdio.h>

#include "hp_smart_card.h"
#include "hp_smart_card_gpio_ifc.h"
#include "hp_smart_card_i2c_ifc.h"
#include "hp_debug_log_internal.h"
#include "hp_assert.h"
#include "hp_generic_macros.h"

#include "hp_host_smart_card_ifc.h"
#include "hp_host_smart_card.h"

//#include <iostream>

//using namespace std;

/**************************************
 * Typedefs
 *************************************/
typedef void(*test_case_func)(void);

typedef struct
{
	char* test_name;
	test_case_func  test_func_ptr;
} test_case_t;

/**************************************
 * Forward decalrations
 *************************************/
void get_library_version(char* id);
void set_log_depth(char* id, unsigned char log_depth);

void host_smart_card_echo(char* id);
void smart_card_echo(char* id);
void device_init(char* id);
void device_close(char* id);

void read_family_id(char* id);
void read_mfg_year(char* id);
void read_ctrdg_mru_year(char* id);
void write_ctrdg_mru_year(char* id, uint32_t fi_year);

void read_ooi_bit(char* id);
void set_ooi_bit(char* id);
void clear_ooi_bit(char* id);

void read_first_install_year(char* id);
void write_first_install_year(char* id, uint32_t mru_year);
void read_wp_fuse(char* id);
void set_wp_fuse(char* id);
void clear_wp_fuse(char* id);
void first_inst_change_part(char* id);

void read_reorder_part_num(char* id);

void read_first_platform_id(char* id);
void write_first_platform_id(char* id);

void library_shutdown(char* id);

/**************************************
 * Static variables
 *************************************/

 /*
 static test_case_t test_case[] = {
	 { "Get Library Version", get_library_version },
	 { "Set Log Depth", set_log_depth },
	 { "Host smart card Echo", host_smart_card_echo },
	 { "Smart card Echo", smart_card_echo },
	 { "DEVICE INIT", device_init },
	 { "Device close", device_close },
	 { "Read Family ID", read_family_id },
	 { "Read manufacture year", read_mfg_year },
	 { "Read MRU year", read_ctrdg_mru_year },
	 { "Write MRU year", write_ctrdg_mru_year },
	 { "Read OOI bit", read_ooi_bit },
	 { "Set OOI bit", set_ooi_bit },
	 { "Clear OOI bit", clear_ooi_bit },
	 { "Read First Install year", read_first_install_year },
	 { "Write First Install year", write_first_install_year },
	 { "Read First Install partition fuse", read_wp_fuse },
	 { "Set First Install partition fuse", set_wp_fuse },
	 { "Clear First Install partition fuse", clear_wp_fuse },
	 { "First Install partition to RO", first_inst_change_part },
	 { "Read Reorder Part num", read_reorder_part_num },
	 { "Read First Platform ID", read_first_platform_id },
	 { "Write First Platform ID", write_first_platform_id },
	 { "Library Shutdown", library_shutdown },
 };
 */

 //#define NUM_TEST_CASES      (sizeof(test_case) / sizeof(test_case_t))
 //#define MENU_COL_GAP        3

static int  menu_col_width;
static int  menu_col2_offset;

#include "carnac_types.h"


void print_returns(HP_SMART_CARD_result_t result)
{

	HW_SMART_CARD_status_t status = LIB_HP_SMART_CARD_last_status();
	printf("Result = %s (%d)  Status = %s (%d)\n",
		LIB_HP_SMART_CARD_result_string(result),
		result,
		LIB_HP_SMART_CARD_status_string(status),
		status);
}
void assert_handler(const char* error_str)
{
	printf("=========================================\n");
	printf("Test Main: HP_ASSERT Failed\n");
	printf("%s\n", error_str);
	printf("=========================================\n");
}
void cache_monitor_failure_handler(HP_SMART_CARD_device_id_t dev_id, HP_SMART_CARD_result_t result)
{
	printf("=========================================\n");
	printf("Test Main: Cache monitor failure\n");
	printf("Device Id = %d, ", dev_id);
	print_returns(result);
	printf("=========================================\n");
}
/*
*/



void init_library(void (*assert_handler_)(const char*), void (*cache_monitor_failure_handler_)(HP_SMART_CARD_device_id_t, HP_SMART_CARD_result_t))
{
	printf("initializing\n");


	HP_SMART_CARD_gpio_init();
	printf("HP_SMART_CARD_gpio_init\n");

	HP_SMART_CARD_i2c_init();
	printf("HP_SMART_CARD_i2c_init\n");


	LIB_HP_SMART_CARD_register_assert_callback(assert_handler_);
	printf("HP_SMART_CARD_i2c_init\n");


	LIB_HP_SMART_CARD_register_cache_monitor_callback(cache_monitor_failure_handler_);
	printf("LIB_HP_SMART_CARD_register_cache_monitor_callback\n");

	LIB_HP_SMART_CARD_init(0);
	printf("LIB_HP_SMART_CARD_init\n");

	printf("OK\n");
}




int main()
{

	/*
	Hello, all tests listed below failed to run with same errors - HP_SMART_CARD_WRITE_ERROR or HP_SMART_CARD_ILLEGAL_ACCESS
		set_ooi_bit
		clear_ooi_bit
		write_first_install_year	//2022
		clear_wp_fuse
		set_wp_fuse
		first_inst_change_part

		All these tests somehow trying to write into the card so maybe it's just readonly?
		And also I wanted to ask if we need these tests?
	*/


	/*
  //char sz[] = "Hello, World!";	//Hover mouse over "sz" while debugging to see its contents
  //cout << sz << endl;	//<================= Put a breakpoint here


  int     user_input;
  char    user_buf[100];
  int     fields_read;

  // Initialise low level libraries. GPIO and I2C
  HP_SMART_CARD_gpio_init();
  HP_SMART_CARD_i2c_init();

  printf("Initializing smart card library....\n");

  // Register for assert callback
  LIB_HP_SMART_CARD_register_assert_callback(assert_handler);

  // Register for cache monitor callack
  LIB_HP_SMART_CARD_register_cache_monitor_callback(cache_monitor_failure_handler);

  // Initialise the library
  LIB_HP_SMART_CARD_init();

  printf("Initializing smart card library....Done.\n");

  // decide formatting information
  //_analyze_menu();

  while (1)
  {
	  // Disaply the available test cases to user
	  //_display_menu();

	  // Wait for the user input
	  scanf("%s", (char*)&user_buf);

	  // exit on 'e'
	  if (user_buf[0] == 'e' || user_buf[0] == 'E')
		  break;

	  // convert string to integer value
	  fields_read = sscanf(user_buf, "%d", &user_input);

	  // check for invalid test number
	  if (fields_read != 1 || user_input < 0 || user_input >= NUM_TEST_CASES)
	  {
		  printf("Invalid entry. Enter 'e' to exit.\n");
		  continue;
	  }

	  // invoke appropriate test function
	  test_case[user_input].test_func_ptr();
  }
	*/

	return 0;
}

#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

//result defines
#define PASS 0
#define FAIL 1
#define ABORTED 2
#define NOT_STARTED 3
#define INFO 4
#define ERROR 5
#define RESULT 6

extern int init_socket();
extern void send_log(char* id, int type, int result, char* message, ...);

char* HP_SMART_CARD_result_map[] = { "HP_SMART_CARD_OK", "HP_SMART_CARD_NOT_INITIALIZED", "HP_SMART_CARD_ERROR", "HP_SMART_CARD_DEVICE_ABSENT", "HP_SMART_CARD_WRITE_ERROR",
"HP_SMART_CARD_READ_ERROR", "HP_SMART_CARD_DATA_NOT_VALID", "HP_SMART_CARD_BAD_SIZE_FOR_TAG", "HP_SMART_CARD_TAG_NOT_FOUND",
"HP_SMART_CARD_DUPLICATE_TAG", "HP_SMART_CARD_BAD_CHIP_ID", "HP_SMART_CARD_INVALID_CHIP_TAG", "HP_SMART_CARD_INVALID_FAMILY_ID",
"HP_SMART_CARD_ILLEGAL_ACCESS", "HP_SMART_CARD_CACHE_NOT_FLUSHED", "HP_SMART_CARD_BAD_TOKEN", "HP_SMART_CARD_BAD_WRITE_PROT",
"HP_SMART_CARD_INVALID_DEVICE_ID", "HP_SMART_CARD_LIB_NOT_INITIALIZED" };



//#define SUPPRESS_REAL_LIB_CALL





void get_library_version(char* id)
{

	send_log(id, RESULT, PASS, "HP SMART CARD Library version = %s\\n", LIB_HP_SMART_CARD_get_version(id));
	return;
}
void set_log_depth(char* id, unsigned char log_depth)
{

	if (log_depth >= 0 && log_depth <= 5)
	{
#ifndef SUPPRESS_REAL_LIB_CALL
		LIB_HP_SMART_CARD_set_log_depth((unsigned char)log_depth);
#endif // !SUPPRESS_REAL_LIB_CALL

		send_log(id, RESULT, PASS, "Set log depth to %d", log_depth);
	}
	else
		send_log(id, RESULT, ABORTED, "depth should be from 0 to 5. You entered %d", log_depth);

}
void host_smart_card_echo(char* id)
{
	HP_SMART_CARD_result_t result = HP_SMART_CARD_ERROR;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_device_present(id, HP_SMART_CARD_DEVICE_HOST);
#endif
	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "LIB_HP_SMART_CARD_device_present | %s", HP_SMART_CARD_result_map[result]);
	else
		send_log(id, RESULT, FAIL, "LIB_HP_SMART_CARD_device_present | %s", HP_SMART_CARD_result_map[result]);
}
void smart_card_echo(char* id)
{
	HP_SMART_CARD_result_t result = HP_SMART_CARD_ERROR;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_device_present(id, HP_SMART_CARD_DEVICE_ID_0);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "LIB_HP_SMART_CARD_device_present | %s", HP_SMART_CARD_result_map[result]);
	else
		send_log(id, RESULT, FAIL, "LIB_HP_SMART_CARD_device_present | %s", HP_SMART_CARD_result_map[result]);

}
void device_init(char* id)
{
	HP_SMART_CARD_result_t result = HP_SMART_CARD_ERROR;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_device_init(id, HP_SMART_CARD_DEVICE_ID_0);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "Intialized HP_SMART_CARD_DEVICE_ID_0 | %s", HP_SMART_CARD_result_map[result]);
	else
		send_log(id, RESULT, FAIL, "Failed to initialize HP_SMART_CARD_DEVICE_ID_0 | %s", HP_SMART_CARD_result_map[result]);
}
void device_close(char* id)
{
	HP_SMART_CARD_result_t result = HP_SMART_CARD_ERROR;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_device_close(id, HP_SMART_CARD_DEVICE_ID_0);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "Closed HP_SMART_CARD_DEVICE_ID_0 | %s", HP_SMART_CARD_result_map[result]);
	else
		send_log(id, RESULT, FAIL, "Failed to close HP_SMART_CARD_DEVICE_ID_0 | %s", HP_SMART_CARD_result_map[result]);
}



void read_family_id(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 family_id;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_read_field(id, HP_SMART_CARD_DEVICE_ID_0, HP_SMART_CARD_FAMILY_ID, sizeof(family_id), &family_id);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "Family id %d", family_id);
	else
		send_log(id, RESULT, FAIL, "Failed to read Family Id | %s", HP_SMART_CARD_result_map[result]);
	*id++ = family_id;
	*id++ = 99;
}


void read_mfg_year(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 mfg_year;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_read_field(id, HP_SMART_CARD_DEVICE_ID_0, HP_SMART_CARD_INK_MANUFACTURE_YEAR, 1, &mfg_year);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "Mfg year =  %d", mfg_year + 2006);
	else
		send_log(id, RESULT, FAIL, "Failed to read MFG Year | %s", HP_SMART_CARD_result_map[result]);
	*id++ = mfg_year;
	*id++ = 99;
	
}
void read_ctrdg_mru_year(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 mru_year;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_read_field(id, HP_SMART_CARD_DEVICE_ID_0, HP_SMART_CARD_INK_CARTRIDGE_MRU_YEAR, 1, &mru_year);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "MRU year = %d", mru_year + 2006);
	else
		send_log(id, RESULT, FAIL, "Failed to read MRU Year | %s", HP_SMART_CARD_result_map[result]);
}
void write_ctrdg_mru_year(char* id, uint32_t mru_year)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;

	if (mru_year < 2006)
	{
		send_log(id, RESULT, ABORTED, "MRU year should be >=2006 you entered %d", mru_year);
		return;
	}

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_write_field(id, HP_SMART_CARD_DEVICE_ID_0, HP_SMART_CARD_INK_CARTRIDGE_MRU_YEAR, 4, &mru_year);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "set MRU year to %d", mru_year + 2006);
	else
		send_log(id, RESULT, FAIL, "Failed to write MRU year | %s", HP_SMART_CARD_result_map[result]);

}
void read_ooi_bit(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 ooi_bit;

#ifndef SUPPRESS_REAL_LIB_CALL

	result = LIB_HP_SMART_CARD_read_field(id, HP_SMART_CARD_DEVICE_ID_0,
		HP_SMART_CARD_INK_OUT_OF_INK_BIT,
		1,
		&ooi_bit);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "OOI bit (OR partition) now %d", ooi_bit);
	else
		send_log(id, RESULT, FAIL, "Failed to read ooi_bit | %s", HP_SMART_CARD_result_map[result]);

	*id++ = ooi_bit;
	*id++ = 99;
}
void set_ooi_bit(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 ooi_bit = 1;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_write_field(id, HP_SMART_CARD_DEVICE_ID_0,
		HP_SMART_CARD_INK_OUT_OF_INK_BIT,
		1,
		&ooi_bit);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "OOI bit (OR partition) SET");
	else
		send_log(id, RESULT, FAIL, "Failed to write ooi_bit (OR partition) | %s", HP_SMART_CARD_result_map[result]);
}
void clear_ooi_bit(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 ooi_bit = 0;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_write_field(id, HP_SMART_CARD_DEVICE_ID_0,
		HP_SMART_CARD_INK_OUT_OF_INK_BIT,
		1,
		&ooi_bit);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "OOI bit (OR partition) CLEAR");
	else
		send_log(id, RESULT, FAIL, "Failed to write ooi_bit (OR partition) | %s", HP_SMART_CARD_result_map[result]);
}
void read_first_install_year(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 fi_year;
	//printf("Call function first intstall with id %d", id);
#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_read_field(id, HP_SMART_CARD_DEVICE_ID_0,
		HP_SMART_CARD_INK_FIRST_PLATFORM_MFG_YEAR,
		1,
		&fi_year);
#endif
	
	printf(" First install year result %d ", fi_year);
	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "First Install year is %d", fi_year + 2006);
	else
		send_log(id, RESULT, FAIL, "Failed to read First Install Year | %s", HP_SMART_CARD_result_map[result]);
	*id++ = fi_year;
	*id++ = 99;
}


void write_first_install_year(char* id, uint32_t fi_year)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;

	if (fi_year < 2006)
	{
		send_log(id, RESULT, ABORTED, "first_install_year should be >=2006 you entered %d", fi_year);
		return;
	}

	fi_year -= 2006;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_write_field(id, HP_SMART_CARD_DEVICE_ID_0, HP_SMART_CARD_INK_FIRST_PLATFORM_MFG_YEAR, 4, &fi_year);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "First Install year is %d", fi_year);
	else
		send_log(id, RESULT, FAIL, "Failed to write First Install Year | %s", HP_SMART_CARD_result_map[result]);
}



void read_wp_fuse(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 fuse;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_read_field(id, HP_SMART_CARD_DEVICE_ID_0,
		HP_SMART_CARD_INK_WRITE_PROTECT,
		1,
		&fuse);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "WP Fuse = %d", fuse);
	else
		send_log(id, RESULT, FAIL, "Failed to read First Install WP Fuse. Error = | %s", HP_SMART_CARD_result_map[result]);
	*id++ = fuse;
	*id++ = 99;
}
void set_wp_fuse(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 fuse = 1;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_write_field(id, HP_SMART_CARD_DEVICE_ID_0,
		HP_SMART_CARD_INK_WRITE_PROTECT,
		1,
		&fuse);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "First Install WP Fuse SET (does NOT change parition to RO)");
	else
		send_log(id, RESULT, FAIL, "Failed to write First Install WP Fuse | %s", HP_SMART_CARD_result_map[result]);
}




void clear_wp_fuse(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 fuse = 0;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_write_field(id, HP_SMART_CARD_DEVICE_ID_0,
		HP_SMART_CARD_INK_WRITE_PROTECT,
		1,
		&fuse);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "WP Fuse CLEARED");
	else
		send_log(id, RESULT, FAIL, "Failed to write First Install WP Fuse | %s", HP_SMART_CARD_result_map[result]);

}


void first_inst_change_part(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	const int               partition = 2;

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_change_RW_partition_to_RO(id, HP_SMART_CARD_DEVICE_ID_0, (uint8_t)partition);
#endif

	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "First Install Partition (%d) changed to RO");
	else
		send_log(id, RESULT, FAIL, "Failed to change partition to RO | %s", HP_SMART_CARD_result_map[result]);
	*id++ = partition;
	*id = 99;

}
void read_reorder_part_num(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 reorder_part_num[12];
	int                     i = 0;

	// Changed Here
	for (i = 0; i < 12; i++)
	{

#ifndef SUPPRESS_REAL_LIB_CALL
		result = LIB_HP_SMART_CARD_read_field(id, HP_SMART_CARD_DEVICE_ID_0,
			HP_SMART_CARD_INK_REORDER_PART_NUM_CHAR_1 + i,
			sizeof(reorder_part_num[i]),
			&reorder_part_num[i]);
#endif

		if (HP_SMART_CARD_OK == result)
			continue;
		else
			break;
	}



	if (result == HP_SMART_CARD_OK)
	{
		send_log(id, INFO, PASS, "Reorder Parts");
		for (i = 0; i < 12; i++)
		{
			send_log(id, INFO, PASS, "%c", (reorder_part_num[i]) + 32);
			*id++ = reorder_part_num[i];
		}
		*id++ = '\0';
		*id++ = 99;
		send_log(id, RESULT, PASS, "you can see 12 reorder parts above");

	}
	else
		send_log(id, RESULT, FAIL, "Failed to read Re-order platform ID | %s", HP_SMART_CARD_result_map[result]);
}
void read_first_platform_id(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 first_platform_id[13];
	int i = 0;
#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_read_string(id, HP_SMART_CARD_DEVICE_ID_0,
		HP_SMART_CARD_INK_MRU_PLATFORM_ID_CHAR_1,
		12,
		first_platform_id);
	first_platform_id[12] = '\0';
#endif

	if (result == HP_SMART_CARD_OK)
	{
		send_log(id, RESULT, PASS, "first_platform_id = %s", first_platform_id);
		for (i = 0; i < 13; i++)*id++ = first_platform_id[i];
	}
	else
		send_log(id, RESULT, FAIL, "Failed to read first_platform_id | %s", HP_SMART_CARD_result_map[result]);
}
void write_first_platform_id(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 first_platform_id[13] = "Test Value {";

#ifndef SUPPRESS_REAL_LIB_CALL
	result = LIB_HP_SMART_CARD_write_string(id, HP_SMART_CARD_DEVICE_ID_0,
		HP_SMART_CARD_INK_MRU_PLATFORM_ID_CHAR_1,
		12,
		first_platform_id);
#endif
	if (result == HP_SMART_CARD_OK)
		send_log(id, RESULT, PASS, "Success! The first_platform_id was written as %s.", first_platform_id);
	else
		send_log(id, RESULT, FAIL, "Failed to write first_platform_id | %s", HP_SMART_CARD_result_map[result]);
}
void library_shutdown(char* id)
{
	LIB_HP_SMART_CARD_shutdown(id);
	send_log(id, RESULT, PASS, "Completed LIB_HP_SMART_CARD_shutdown()");
}

void warp_read_fild(char* id)
{
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 data_buffer[12];
	int                     i = 0;
	uint8_t                 vid[4];

	vid[0] = 1;
	vid[1] = 0;


	uint8_t fild_address = *id++;
	uint8_t fild_size = *id;
	*id--;// return ptr to start position 
	
	// Changed Here
	printf("call lib read fild add= %d , len= %d \r\n", fild_address, fild_size);
	

	for (i = 0; i < fild_size; i++)
	{

#ifndef SUPPRESS_REAL_LIB_CALL
		result = LIB_HP_SMART_CARD_read_field(vid, HP_SMART_CARD_DEVICE_ID_0,
			(HP_SMART_CARD_field_t)fild_address + i,
			1,
			&data_buffer[i]);
#endif

		if (HP_SMART_CARD_OK == result)
			continue;
		else
		{
			printf("\r\n*****Not Reciev smart card OK************\r\n");
			break;
		}
	}



	if (result == HP_SMART_CARD_OK)
	{
		send_log(id, INFO, PASS, "Reorder Parts");
		for (i = 0; i < fild_size; i++)
		{
			send_log(id, INFO, PASS, "%c", (data_buffer[i]) + 32);
			*id++ = data_buffer[i];
			printf("Result-%d=%d\r\n", i, data_buffer[i]);
		}
		//*id++ = '\0';
		*id++ = 99;
		send_log(id, RESULT, PASS, "you can see 12 reorder parts above");

	}
	else
	{
		printf("\r\n*****Not Reciev smart card OK************\r\n");
		*id++ = 99;
		*id++ = 99;
		send_log(id, RESULT, FAIL, "Failed to read Re-order platform ID | %s", HP_SMART_CARD_result_map[result]);
	}
	
	LIB_HP_SMART_CARD_device_close(vid, HP_SMART_CARD_DEVICE_ID_0);
	printf("\r\n Connection to lib close sucess \r\n");
}


void warp_write_fild(char* id)
{

HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
uint8_t                 filednum = *id++;
uint8_t                 value[4];
uint8_t                 vid[4];
uint8_t wsize = 1;

vid[0] = 1;
vid[1] = 0;
value[0] = *id++;
	
	

printf("Write to %d , data %d  \r\n", filednum, value[0]);
#ifndef SUPPRESS_REAL_LIB_CALL
result = LIB_HP_SMART_CARD_write_field(&vid[0], HP_SMART_CARD_DEVICE_ID_0,
	(HP_SMART_CARD_field_t)filednum,
	wsize, 
	&value);
#endif

if (result == HP_SMART_CARD_OK)
send_log(id, RESULT, PASS, "WP Fuse CLEARED");
else {
	send_log(id, RESULT, FAIL, "Failed to write First Install WP Fuse | %s", HP_SMART_CARD_result_map[result]);
	*id++ = 'E';
	*id++ = result;
	}
	
	LIB_HP_SMART_CARD_device_close(vid, HP_SMART_CARD_DEVICE_ID_0);
	printf("\r\n Connection to lib close sucess \r\n");
}



void wrap_set_debug_level(char* id)
{
	printf("Debug Level Set to %d\r\n", *id);
	LIB_HP_SMART_CARD_set_log_depth(*id);


}
		
void wrap_HP_SMART_CARD_read(char* id)
{
		
	HP_SMART_CARD_result_t  result = HP_SMART_CARD_ERROR;
	uint8_t                 data_buffer[12];
	int                     i = 0;
	uint8_t                 vid[4];

	vid[0] = 1;
	vid[1] = 0;


	uint8_t fild_address = *id++;
	uint8_t fild_size = *id;
	*id--; // return ptr to start position 
	printf("\r\n********************\r\n Size to read is %d \r\n*************\r\n", fild_size);
	if (fild_size > 10)fild_size = 1;
	result = LIB_HP_SMART_CARD_read(vid,
		HP_SMART_CARD_DEVICE_ID_0,
		fild_address ,
		fild_size,
		&data_buffer[i]);
	
	
	if (result == HP_SMART_CARD_OK)
	{
		send_log(id, INFO, PASS, "Reorder Parts");
		for (i = 0; i < fild_size; i++)
		{
			send_log(id, INFO, PASS, "%c", (data_buffer[i]) + 32);
			*id++ = data_buffer[i];
			printf("Result-%d=%d\r\n", i, data_buffer[i]);
		}
		//*id++ = '\0';
		*id++ = 99;
		send_log(id, RESULT, PASS, "you can see 12 reorder parts above");

	}
	else
	{
		printf("\r\n*****Not Reciev smart card OK************\r\n");
		*id++ = 99;
		*id++ = 99;
		send_log(id, RESULT, FAIL, "Failed to read Re-order platform ID | %s", HP_SMART_CARD_result_map[result]);
	}
	
	LIB_HP_SMART_CARD_device_close(vid, HP_SMART_CARD_DEVICE_ID_0);
	
			
}
		