#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdarg.h>

#define SOCKET_INIT_ERROR (int)(2)
#define SOCKET_CONNECT_ERROR (int)(3)

static int global_socket = 0;

int init_socket()
{
	struct sockaddr_in serv_addr;
	memset(&serv_addr, '0', sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(1545);

	if ((global_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		//printf("SOCKET_INIT_ERROR\n");
		global_socket = 0;
		return SOCKET_INIT_ERROR;
	}

	if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
	{
		//printf("SOCKET_INIT_ERROR\n");
		global_socket = 0;
		return SOCKET_INIT_ERROR;
	}

	if (connect(global_socket, (const struct sockaddr_in*)&serv_addr, sizeof(serv_addr)) < 0)
	{
		//printf("SOCKET_CONNECT_ERROR\n");
		global_socket = 0;
		return SOCKET_CONNECT_ERROR;
	}

	return 0;
}

//result defines
#define PASS 0
#define FAIL 1
#define ABORTED 2
#define NOT_STARTED 3
#define INFO 4
#define ERROR 5
#define RESULT 6

char* result_define_map[7] = { "PASS", "FAIL", "ABORTED", "NOT_STARTED", "info", "error", "result" };

void send_log(char* id, int type, int result, char* message, ...)
{
	int sockStatus = 0;

	if (global_socket == 0)
	{
		sockStatus = init_socket();
	}

	if (sockStatus == 0 && id != NULL)
	{

		int final_message_size = 34 + strlen(message) + 256;
		char* msg = (char*)malloc(final_message_size);
		memset(msg, 0, final_message_size);

		char* message_ = (char*)malloc(2048);
		va_list args;
		va_start(args, message);
		vsprintf(message_, message, args);
		va_end(args);

		sprintf(msg, "{\"id\":\"%s\",\"type\":\"%s\",\"log\":\"%s\",\"result\":\"%s\"}(!@#$%^&*&^%$#@!)\0", id, result_define_map[type], message_, result_define_map[result]);

		send(global_socket, msg, strlen(msg), MSG_DONTWAIT);

		free(msg);
		free(message_);
	}
}